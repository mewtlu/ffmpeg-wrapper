const fs = require('fs');
const { spawn } = require('child_process');
const { getVideoDurationInSeconds } = require('get-video-duration')

class Drawtext
{
    constructor(strInputFile, strOutputFile, strText, strStartX, strStartY, strTextScrollRatio, strFontFile)
    {
        this.testDependencies();

        this.inputFile = strInputFile;
        this.outputFile = strOutputFile;
        this.text = strText;
        this.startX = strStartX;
        this.startY = strStartY;
        this.textScrollRatio = strTextScrollRatio;
        this.fontFile = strFontFile;

        this.commandsFile = this.inputFile.replace('.mp4', '-commands.txt');

        this.ready = getVideoDurationInSeconds(this.inputFile).then((strDuration) => {
            this.duration = strDuration;
        });

        return this;
    }

    set inputFile (strInputFile)
    {
        return this._inputFile = strInputFile;
    }
    get inputFile ()
    {
        return this._inputFile;
    }

    set outputFile (strOutputFile)
    {
        return this._outputFile = strOutputFile;
    }
    get outputFile ()
    {
        return this._outputFile;
    }

    set text (strText)
    {
        return this._text = strText;
    }
    get text ()
    {
        return this._text;
    }

    set textScrollRatio (strTextScrollRatio)
    {
        return this._textScrollRatio = strTextScrollRatio;
    }
    get textScrollRatio ()
    {
        return this._textScrollRatio;
    }

    set startX (strStartX)
    {
        return this._startX = strStartX;
    }
    get startX ()
    {
        return this._startX;
    }

    set startY (strStartY)
    {
        return this._startY = strStartY;
    }
    get startY ()
    {
        return this._startY;
    }

    set commandsFile (strCommandsFile)
    {
        return this._commandsFile = strCommandsFile;
    }
    get commandsFile ()
    {
        return this._commandsFile;
    }

    set duration (strDuration)
    {
        return this._duration = strDuration;
    }
    get duration ()
    {
        return this._duration;
    }

    set ready (promiseReady)
    {
        return this._promiseReady = promiseReady;
    }
    get ready ()
    {
        return this._promiseReady;
    }

    set fontFile (strFontFile)
    {
        return this._fontFile = strFontFile;
    }
    get fontFile ()
    {
        return this._fontFile;
    }

    testDependencies()
    {
        let child = spawn(
            'ffmpeg',
            [
                '--help'
            ]
        );

        let fnErrorHandler = (code) => {
            if (code !== 0) {
                console.error('FFMPEG could not be found on the host machine.');
                process.exit();
            }
        };

        child.on('exit', fnErrorHandler);
        child.on('error', fnErrorHandler);
    }

    createCommands()
    {
        let strOut = '';
        let strTemplate = "{{time}}   drawtext     reinit  \'x={{x}}:y={{y}}\';\n";

        for (let time = 0; time <= this.duration; time += this.duration * 0.001) {
            let x = this.startX - (this.textScrollRatio * time);
            strOut += strTemplate
                .replace('{{time}}', Math.round(100 * time) / 100)
                .replace('{{x}}', Math.round(x, 0))
                .replace('{{y}}', this.startY);
        }

        fs.writeFileSync(this.commandsFile, strOut);

        return this;
    }

    createRender()
    {
        let strCmd = `/usr/local/bin/ffmpeg`;
        let arrTextStyle = [
            `drawtext=fontfile=${this.fontFile}`,
            `text=${this.text}`,
            `fontcolor=#071869`,
            `shadowx=5`,
            `shadowy=5`,
            `shadowcolor=#21212655`,
            `fontsize=64:`,
        ];
        let arrArgs = [
            '-y',
            '-i',
            this.inputFile,
            '-vf',
            `sendcmd=f=${this.commandsFile},` +
                arrTextStyle.join(': '),
            '-codec:a',
            'copy',
            this.outputFile
        ];
        console.log(`${strCmd} ${arrArgs.join(' ')}`);

        let child = spawn(
            strCmd,
            arrArgs
        );

        child.stdout.on('data', function (data) {
            console.log('stdout: ' + data.toString());
        });

        child.stderr.on('data', function (data) {
            console.log('stderr: ' + data.toString());
        });

        child.on('exit', function (code) {
            console.log('child process exited with code ' + code.toString());
        });

        return this;
    }
}


let video = new Drawtext(process.argv[2], process.argv[3], process.argv[4], process.argv[5], process.argv[6], process.argv[7], process.argv[8])


video.ready
    .then(() => {
        video.createCommands();
        video.createRender();
    })
