# Node FFMPEG Wrapper

## Intention

Allows using FFMPEG and its drawtext function to overlay moving text onto a video at a configurable rate.

## Installation

- Clone the repo
- Requires `ffmpeg` to be installed on host
   - Mac: `brew install ffmpeg`  

## Usage

Syntax: `node . <input file> <output file> <text> <text start x> <text start y> <text scroll speed> <font path>`  
Example: `node . example/test.mp4 example/output.mp4 "Sample Text" 1050 650 20 example/Arial.ttf`